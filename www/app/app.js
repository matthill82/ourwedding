angular.module('OurWeddingApp', ['ionic', "uiGmapgoogle-maps"])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
	  cordova.plugins.Keyboard.disableScroll();
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

	  .state('/intro', {
		  url: "/intro",
		  templateUrl: "app/intro/intro.html",
		  controller: "IntroController"
	  })

	  .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "app/layout/menu-layout.html",
      controller: 'LoginController' // Login controller
    })

	  .state('app.about', {
		  url: "/about",
		  views: {
			  'menuContent' : {
				  templateUrl: "app/about/about.html"
			  }
		  }
	  })

	  .state('app.menu', {
		  url: "/menu",
		  abstract: true,
		  views: {
			  'menuContent' : {
				  templateUrl: "app/menu/menu.html"
			  }
		  }
	  })

      .state('app.menu.starter', {
          url: "/starter",
          views: {
              'tab-starter' : {
                  templateUrl: "app/menu/starter.html"
              }
          }
      })
      .state('app.menu.starterId', {
          url: "/starter/:id",
          views: {
              'tab-starter' : {
                  templateUrl: "app/menu/starter-item.html"
              }
          }
      })

      .state('app.menu.main', {
          url: "/main",
          views: {
              'tab-main' : {
                  templateUrl: "app/menu/main.html"
              }
          }
      })


      .state('app.menu.mainId', {
          url: "/main/:id",
          views: {
              'tab-main' : {
                  templateUrl: "app/menu/main-item.html"
              }
          }
      })

      .state('app.menu.evening', {
          url: "/evening",
          views: {
              'tab-evening' : {
                  templateUrl: "app/menu/evening.html"
              }
          }
      })

      .state('app.menu.eveningId', {
          url: "/evening/:id",
          views: {
              'tab-evening' : {
                  templateUrl: "app/menu/evening-item.html"
              }
          }
      })

      .state('app.venue', {
          url: "/venue",
		  abstract: true,
          views: {
              'menuContent' : {
                  templateUrl: "app/venue/venue.html"
              }
          }
      })

	  .state('app.venue.church', {
		  url: "/church",
		  views: {
			  'tab-church' : {
				  templateUrl: "app/venue/church.html"
			  }
		  }
	  })

	  .state('app.venue.church-map', {
		  url: "/church-map",
		  views: {
			  'tab-church' : {
				  templateUrl: "app/venue/church-map.html"
			  }
		  }
	  })

	  .state('app.venue.reception', {
		  url: "/reception",
		  views: {
			  'tab-reception' : {
				  templateUrl: "app/venue/reception.html"
			  }
		  }
	  })

	  .state('app.venue.reception-map', {
		  url: "/reception-map",
		  views: {
			  'tab-reception' : {
				  templateUrl: "app/venue/reception-map.html"
			  }
		  }
	  })

      .state('app.overnight', {
          url: "/overnight",
          abstract: true,
          views: {
              'menuContent' : {
                  templateUrl: "app/overnight/overnight.html"
              }
          }
      })

      .state('app.overnight.hotels', {
          url: "/overnight",
          views: {
              'tab-hotels' : {
                  templateUrl: "app/overnight/hotels.html"
              }
          }
      })

      .state('app.overnight.hotel-map', {
          url: "/hotel-map/:id",
          views: {
              'tab-hotels' : {
                  templateUrl: "app/overnight/hotel-map.html"
              }
          }
      })

	  .state('app.overnight.taxis', {
		  url: "/taxis",
		  views: {
			  'tab-taxis' : {
				  templateUrl: "app/overnight/taxis.html"
			  }
		  }
	  })

	  .state('app.contact', {
		  url: "/contact",
		  views: {
			  'menuContent' : {
				  templateUrl: "app/contact/contact.html"
			  }
		  }
	  });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/intro');
});

