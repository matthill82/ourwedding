(function () {
	'use strict';

	angular.module('OurWeddingApp').controller('HotelMapController', ['$stateParams', 'feedApi', HotelMapController]);

	function HotelMapController($stateParams, feedApi) {

		var vm = this;

		vm.hotelId = Number($stateParams.id);

		feedApi.getOvernightData().then(function(data) {

            vm.map = {
                center : {
                    latitude : data.hotels[vm.hotelId].latitude,
                    longitude :  data.hotels[vm.hotelId].longitude
                },
                zoom : 16
            };

            console.log(data.hotels[vm.hotelId].latitude);

            vm.marker = {
                latitude: data.hotels[vm.hotelId].latitude,
                longitude: data.hotels[vm.hotelId].longitude,
                idKey: data.hotels[vm.hotelId].id,
                title: data.hotels[vm.hotelId].title + "<br/>(Tap for directions)",
                showWindow: true
            };

		});


        vm.locationClicked = function (marker) {
            window.location = "geo:" + marker.latitude + "," + marker.longitude;
        };

	}

})();