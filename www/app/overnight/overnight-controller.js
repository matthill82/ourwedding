/**
 * Created by mhill168 on 08/12/14.
 */

(function () {
	'use strict';

	angular.module('OurWeddingApp').controller('OvernightController', ['$state','feedApi', OvernightController]);

	function OvernightController($state, feedApi) {

		var vm = this;

		feedApi.getOvernightData().then(function (data) {
            vm.hotels = data.hotels;
            vm.taxis = data.taxis;
        });

	}

})();
