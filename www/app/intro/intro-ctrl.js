/**
 * Created by mhill168 on 07/12/14.
 */

(function () {

	'use strict';

	angular.module('OurWeddingApp').controller('IntroController', ['$state', '$scope', '$ionicNavBarDelegate', IntroController]);

    function IntroController ($state, $scope, $ionicNavBarDelegate) {

		var vm = this;

		// Called to navigate to the main app
		$scope.startApp = function() {

			// Disable back button on this controller
			$ionicNavBarDelegate.showBackButton(false);

			$state.go('app.about');

			// Set a flag that we finished the tutorial
			window.localStorage['didIntro'] = false; // for showcase
		};

		// Check if the user already did the intro and skip it if so
		if(window.localStorage['didIntro'] === "true") {
			console.log('Skip intro');
			$scope.startApp();
		}
		else{
			setTimeout(function () {}, 750);
		}

		// Move to the next slide
		$scope.next = function() {
			$scope.$broadcast('slideBox.nextSlide');
		};

        $scope.previous = function () {
            $scope.$broadcast('slideBox.prevSlide');

        };

		// Called each time the slide changes
		$scope.slideChanged = function (index) {
            $scope.slideIndex = index;
            console.log(index);

		};
	}

})();
