(function () {
	'use strict';

	angular.module('OurWeddingApp').controller('ReceptionMapController', ['$stateParams', 'feedApi', ReceptionMapController]);

	function ReceptionMapController($stateParams, feedApi) {

		var vm = this;

		vm.receptionId = Number($stateParams.id);

		feedApi.getVenuesData().then(function(data) {

			vm.map = {
				center : {
					latitude : data.reception.latitude,
					longitude :  data.reception.longitude
				},
				zoom : 12
			};

            vm.marker = {
                latitude: data.reception.latitude,
                longitude: data.reception.longitude,
                idKey: data.reception.id,
                title: data.reception.title + "<br/>(Tap for directions)",
                showWindow: true
            };
		});


        vm.locationClicked = function (marker) {
            window.location = "geo:" + marker.latitude + "," + marker.longitude;
        };

		function findById(item) {
			return item.id === vm.receptionId;
		}

	}

})();