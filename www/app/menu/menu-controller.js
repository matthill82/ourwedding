/**
 * Created by mhill168 on 08/12/14.
 */

(function () {
	'use strict';

	angular.module('OurWeddingApp').controller('MenuController', ['$scope','feedApi', '$ionicPopup', '$timeout', MenuController]);

	function MenuController($scope, feedApi, $ionicPopup, $timeout) {

        // TODO: sort out the tabs and set up the different meal states

		var vm = this;

		feedApi.getMenuData().then(function(data) {
            vm.mains = data.mains;
            vm.starters = data.starters;

            /**
             * evening menu
             */
            for(var val in data.evening) {
                if(data.evening.hasOwnProperty(val)) {
                    vm.evenings = data.evening[val];
                }
            }
        });

        /**
         * This will be for the favourites
         */

        $scope.isGrey = [];

        $scope.toggleClass = function ($index) {
            $scope.isGrey[$index] = $scope.isGrey[$index]=='error'?'warning':'error';
        };

            // Triggered on a button click, or some other target
        $scope.showPopup = function() {
            var myPopup = $ionicPopup.show({
                template: '',
                title: 'Your meal is added to your Favourites',
                scope: $scope,
                buttons: [
                    {
                        text: '<b>Ok</b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            myPopup.close();
                        }
                    }
                ]
            });
        };



        /*
         * if given group is the selected group, deselect it
         * else, select the given group
         */
        $scope.toggleGroup = function(group) {
            if ($scope.isGroupShown(group)) {
                $scope.shownGroup = null;
            } else {
                $scope.shownGroup = group;
            }
        };
        $scope.isGroupShown = function(group) {
            return $scope.shownGroup === group;
        };

	}

})();
