(function () {
	'use strict';

	angular.module('OurWeddingApp').controller('StarterMealController', ['$stateParams', 'feedApi', StarterController]);

	function StarterController($stateParams, feedApi) {

		var vm = this;

		vm.starterId = Number($stateParams.id);

        feedApi.getMenuData().then(function(data) {
            vm.starter = _.chain(data.starters)
                .filter(findStarterId)
                .map(function (item) {
                    return {
                        id: item.id,
                        name: item.name,
                        description: item.description,
                        imgSrc: item.imgSrc
                    }
                })
                .value();
        });

        function findStarterId(item) {
            return item.id === vm.starterId;
        }

		console.log("$stateParams", $stateParams);

	}

})();